﻿
$("#gridUser").kendoGrid({
    dataSource: {
        type: "JSON",
        transport: {
            read: "/User/GetAll/"
        },
        schema: {
            model: {
                id: "Id"
            }
        },
        pageSize: 10
    },
    columns: [
        {
            selectable: true, width: "35px"
        },
        {
            field: "UserName", title: "Tài khoản", width: "150px"
        },
        {
            field: "Email", title: "Email", width: "200px"
        },
        {
            field: "PhoneNumber", title: "Điện thoại", width: "100px"
        },
        {
            field: "Address", title: "Địa chỉ"
        },
        {
            field: "", title: "#", width: "65px",
            template: kendo.template($("#modifyTemplateUser").html())
        }
    ],
    //height: 400,
    scrollable: false,
    sortable: true,
    //persistSelection: true,
    resizable: true,
    pageable: {
        refresh: true,
        pageSizes: true,  // Hiển thị Combobox hiển thị bao nhiêu bản ghi
        buttonCount: 5,  // Hiển thị có bao nhiêu chỉ số pages được hiện ra
        messages: {
            itemsPerPage: "dòng mỗi trang",
            display: " {0} - {1} của {2} dòng",
            empty: "Không có dữ liệu nào trong bảng",
            refresh: "Tải lại",
            first: "Trang đầu",
            last: "Trang cuối",
            previous: "Trang trước",
            next: "Trang sau"
        }
    }
});