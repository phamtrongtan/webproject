﻿
$("#gridPermission").kendoGrid({
    dataSource: {
        type: "odata",
        transport: {
            read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Customers"
        },
        schema: {
            model: {
                id: "CustomerID"
            }
        },
        pageSize: 5
    },
    columns: [
        {
            selectable: true, width: "35px"
        },
        {
            field: "ContactName", title: "Contact Name", width: "240px"
        },
        {
            field: "ContactTitle", title: "Contact Title"
        },
        {
            field: "CompanyName", title: "Company Name"
        },
        {
            field: "Country", width: "60px"
        },
        {
            field: "", title: "#", width: "65px",
            template: kendo.template($("#modifyTemplatePermission").html())
        }
    ],
    //height: 400,
    scrollable: false,
    sortable: true,
    //persistSelection: true,
    resizable: true,
    pageable: {
        refresh: true,
        pageSizes: true,  // Hiển thị Combobox hiển thị bao nhiêu bản ghi
        buttonCount: 5,  // Hiển thị có bao nhiêu chỉ số pages được hiện ra
        messages: {
            itemsPerPage: "dòng mỗi trang",
            display: " {0} - {1} của {2} dòng",
            empty: "Không có dữ liệu nào trong bảng",
            refresh: "Tải lại",
            first: "Trang đầu",
            last: "Trang cuối",
            previous: "Trang trước",
            next: "Trang sau"
        }
    }
});