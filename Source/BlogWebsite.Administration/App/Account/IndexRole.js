﻿
$("#gridRole").kendoGrid({
    dataSource: {
        type: "JSON",
        transport: {
            read: "/Role/GetAll/"
        },
        schema: {
            model: {
                id: "Id"
            }
        },
        pageSize: 10
    },
    columns: [
        {
            selectable: true, width: "35px"
        },
        {
            field: "RoleName", title: "Vai trò", width: "300px"
        },
        {
            field: "Description", title: "Mô tả"
        },
        {
            field: "", title: "#", width: "95px",
            template: kendo.template($("#modifyTemplateRole").html())
        }
    ],
    //height: 400,
    scrollable: false,
    sortable: true,
    //persistSelection: true,
    resizable: true,
    pageable: {
        refresh: true,
        pageSizes: true,  // Hiển thị Combobox hiển thị bao nhiêu bản ghi
        buttonCount: 5,  // Hiển thị có bao nhiêu chỉ số pages được hiện ra
        messages: {
            itemsPerPage: "dòng mỗi trang",
            display: " {0} - {1} của {2} dòng",
            empty: "Không có dữ liệu nào trong bảng",
            refresh: "Tải lại",
            first: "Trang đầu",
            last: "Trang cuối",
            previous: "Trang trước",
            next: "Trang sau"
        }
    }
});