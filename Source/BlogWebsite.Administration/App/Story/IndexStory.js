﻿
$("#gridStory").kendoGrid({
    dataSource: {
        type: "JSON",
        transport: {
            read: {
                url: "/Story/Search",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8"
            },
            parameterMap: function (data, type) {
                if (type == "read") {
                    if ($('#txtSearch').val() == null) data.keyword = "";
                    else data.keyword = $('#txtSearch').val();
                    
                    return JSON.stringify(data);
                }
            }
        },
        schema: {
            model: {
                id: "Id"
            }
        },
        pageSize: 10
    },
    columns: [
        {
            selectable: true, width: "35px"
        },
        {
            field: "StoryName", title: "Tên truyện"
        },
        {
            field: "Author", title: "Tác giả", width: "150px"
        },
        {
            field: "StoryTypeName", title: "Thể loại", width: "100px"
        },
        {
            field: "ChapterNumber", title: "Số chương", width: "90px"
        },
        {
            field: "Status", title: "Trạng thái", width: "90px"
        },
        {
            field: "", title: "#", width: "95px",
            template: kendo.template($("#modifyTemplateStory").html())
        }
    ],
    //height: 400,
    scrollable: false,
    sortable: true,
    //persistSelection: true,
    resizable: true,
    pageable: {
        refresh: true,
        pageSizes: true,  // Hiển thị Combobox hiển thị bao nhiêu bản ghi
        buttonCount: 5,  // Hiển thị có bao nhiêu chỉ số pages được hiện ra
        messages: {
            itemsPerPage: "dòng mỗi trang",
            display: " {0} - {1} của {2} dòng",
            empty: "Không có dữ liệu nào trong bảng",
            refresh: "Tải lại",
            first: "Trang đầu",
            last: "Trang cuối",
            previous: "Trang trước",
            next: "Trang sau"
        }
    }
});

$("#btnSearch").click(function () {
    $("#gridStory").data("kendoGrid").dataSource.read();
});

