﻿
$("#gridStoryType").kendoGrid({
    dataSource: {
        type: "JSON",
        transport: {
            read: {
                url: "/StoryType/Search",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8"
            },
            parameterMap: function (data, type) {
                if (type == "read") {
                    if ($('#txtSearch').val() == null) data.keyword = "";
                    else data.keyword = $('#txtSearch').val();

                    return JSON.stringify(data);
                }
            }
        },
        schema: {
            model: {
                id: "Id"
            }
        },
        pageSize: 5
    },
    columns: [
        {
            selectable: true, width: "35px"
        },
        {
            field: "StoryTypeName", title: "Thể loại", width: "50%"
        },
        {
            field: "Description", title: "Ghi chú"
        },
        {
            field: "", title: "#", width: "65px",
            template: kendo.template($("#modifyTemplateStoryType").html())
        }
    ],
    //height: 400,
    scrollable: false,
    sortable: true,
    //persistSelection: true,
    resizable: true,
    pageable: {
        refresh: true,
        pageSizes: true,  // Hiển thị Combobox hiển thị bao nhiêu bản ghi
        buttonCount: 5,  // Hiển thị có bao nhiêu chỉ số pages được hiện ra
        messages: {
            itemsPerPage: "dòng mỗi trang",
            display: " {0} - {1} của {2} dòng",
            empty: "Không có dữ liệu nào trong bảng",
            refresh: "Tải lại",
            first: "Trang đầu",
            last: "Trang cuối",
            previous: "Trang trước",
            next: "Trang sau"
        }
    }
});

$("#btnSearch").click(function () {
    $("#gridStoryType").data("kendoGrid").dataSource.read();
});

