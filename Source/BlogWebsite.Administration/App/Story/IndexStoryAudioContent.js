﻿
$("#gridStoryAudioContent").kendoGrid({
    dataSource: {
        type: "odata",
        transport: {
            read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Customers"
        },
        schema: {
            model: {
                id: "CustomerID"
            }
        },
        pageSize: 5
    },
    columns: [
        {
            selectable: true, width: "35px"
        },
        {
            field: "ContactName", title: "Tên chương"
        },
        {
            field: "", title: "#", width: "95px",
            template: kendo.template($("#modifyTemplateStoryAudioContent").html())
        }
    ],
    //height: 400,
    scrollable: false,
    sortable: true,
    //persistSelection: true,
    resizable: true,
    pageable: {
        refresh: true,
        pageSizes: true,  // Hiển thị Combobox hiển thị bao nhiêu bản ghi
        buttonCount: 5,  // Hiển thị có bao nhiêu chỉ số pages được hiện ra
        messages: {
            itemsPerPage: "dòng mỗi trang",
            display: " {0} - {1} của {2} dòng",
            empty: "Không có dữ liệu nào trong bảng",
            refresh: "Tải lại",
            first: "Trang đầu",
            last: "Trang cuối",
            previous: "Trang trước",
            next: "Trang sau"
        }
    }
});


$("#detailPopup").kendoWindow({
    width: "970px",
    title: "Đọc truyện",
    visible: false,
    actions: [
        "Minimize",
        "Maximize",
        "Close"
    ]
});


function ShowDetailBook(duieu) {

    // Lấy BookId
    var data = duieu.getAttribute("data-StoryAudioContentId");
    var bookname = duieu.getAttribute("data-BookName");
    $("#detailPopup").data("kendoWindow").title(bookname);

    $.ajax({
        url: "/StoryContent/Watch",
        dataType: "html",
        data: { id: data },
        success: function (result) {
            $('#popup_Container').html(result);

            console.log(result);
        }
    });

    $("#detailPopup").data("kendoWindow").center().open();
}


$("#btnCancelPopup").click(function (e) {
    e.preventDefault();
    $("#detailPopup").data("kendoWindow").center().close();
});