﻿
$("#gridStoryAudio").kendoGrid({
    dataSource: {
        type: "odata",
        transport: {
            read: "https://demos.telerik.com/kendo-ui/service/Northwind.svc/Customers"
        },
        schema: {
            model: {
                id: "CustomerID"
            }
        },
        pageSize: 5
    },
    columns: [
        {
            selectable: true, width: "35px"
        },
        {
            field: "ContactName", title: "Tên truyện"
        },
        {
            field: "ContactTitle", title: "Tác giả", width: "100px"
        },
        {
            field: "CompanyName", title: "Thể loại", width: "100px"
        },
        {
            field: "Country", title: "Số chương", width: "90px"
        },
        {
            field: "Country", title: "Trạng thái", width: "90px"
        },
        {
            field: "", title: "#", width: "95px",
            template: kendo.template($("#modifyTemplateStoryAudio").html())
        }
    ],
    //height: 400,
    scrollable: false,
    sortable: true,
    //persistSelection: true,
    resizable: true,
    pageable: {
        refresh: true,
        pageSizes: true,  // Hiển thị Combobox hiển thị bao nhiêu bản ghi
        buttonCount: 5,  // Hiển thị có bao nhiêu chỉ số pages được hiện ra
        messages: {
            itemsPerPage: "dòng mỗi trang",
            display: " {0} - {1} của {2} dòng",
            empty: "Không có dữ liệu nào trong bảng",
            refresh: "Tải lại",
            first: "Trang đầu",
            last: "Trang cuối",
            previous: "Trang trước",
            next: "Trang sau"
        }
    }
});

