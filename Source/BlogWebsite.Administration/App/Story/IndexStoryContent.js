﻿
$("#gridStoryContent").kendoGrid({
    dataSource: {
        type: "JSON",
        transport: {
            read: {
                url: "/Story/Contents_Index/",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8"
            },
            parameterMap: function (data, type) {
                if (type == "read") {
                    if ($('#txtSearch').val() == null) data.keyword = "";
                    else data.keyword = $('#txtSearch').val();

                    return JSON.stringify(data);
                }
            }
        },
        schema: {
            model: {
                id: "Id"
            }
        },
        pageSize: 5
    },
    columns: [
        {
            selectable: true, width: "35px"
        },
        {
            field: "ChapterName", title: "Tên chương"
        },
        {
            field: "", title: "#", width: "95px",
            template: kendo.template($("#modifyTemplateStoryContent").html())
        }
    ],
    //height: 400,
    scrollable: false,
    sortable: true,
    //persistSelection: true,
    resizable: true,
    pageable: {
        refresh: true,
        pageSizes: true,  // Hiển thị Combobox hiển thị bao nhiêu bản ghi
        buttonCount: 5,  // Hiển thị có bao nhiêu chỉ số pages được hiện ra
        messages: {
            itemsPerPage: "dòng mỗi trang",
            display: " {0} - {1} của {2} dòng",
            empty: "Không có dữ liệu nào trong bảng",
            refresh: "Tải lại",
            first: "Trang đầu",
            last: "Trang cuối",
            previous: "Trang trước",
            next: "Trang sau"
        }
    }
});

$("#btnSearch").click(function () {
    $("#gridStoryContent").data("kendoGrid").dataSource.read();
});


$("#detailPopup").kendoWindow({
    width: "970px",
    height: "500px",
    title: "Đọc truyện",
    modal: true,
    visible: false,
    scrollable: true,
    actions: [
        "Minimize",
        "Maximize",
        "Close"
    ]
});

$("#deleteConfirm").kendoWindow({
    width: "400px",
    //height: "500px",
    title: "Thông báo",
    modal: true,
    visible: false,
    scrollable: true,
    actions: [
        "Minimize",
        "Maximize",
        "Close"
    ]
});


function ShowDetailBook(duieu) {

    // Lấy BookId
    var data = duieu.getAttribute("data-StoryContentId");
    $("#detailPopup").data("kendoWindow").title("Nội dung chương");

    $.ajax({
        url: "/StoryContent/Watch",
        dataType: "html",
        data: { id: data },
        success: function (result) {
            $('#popup_Container').html(result);
        }
    });

    $("#detailPopup").data("kendoWindow").center().open();
}

$("#btnCancelPopup").click(function (e) {
    e.preventDefault();
    $("#detailPopup").data("kendoWindow").center().close();
});


var deleteId = 0;
function btnDelete(duieu) {

    // Lấy BookId
    deleteId = duieu.getAttribute("data-StoryContentId");
    $("#deleteConfirm").data("kendoWindow").title("Thông báo");

    $("#deleteConfirm").data("kendoWindow").center().open();
}


$("#btnAgreePopup").click(function (e) {

    $.ajax({
        url: "/StoryContent/Delete",
        data: { id: deleteId },
        success: function (result) {
            $("#deleteConfirm").data("kendoWindow").center().close();  
            location.reload();
        }
    });
});

$("#btnExitPopup").click(function (e) {
    e.preventDefault();
    $("#deleteConfirm").data("kendoWindow").center().close(); 
});

