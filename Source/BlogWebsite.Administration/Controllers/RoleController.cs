﻿using BlogWebsite.Administration.Models.ViewModel;
using BlogWebsite.Data.Business.Account;
using BlogWebsite.Data.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Controllers
{
    public class RoleController : Controller
    {
        RoleBusiness roleBusiness;
        PermissionBusiness permissionBusiness;

        public RoleController()
        {
            roleBusiness = new RoleBusiness();
            permissionBusiness = new PermissionBusiness();
        }

        [HttpGet]
        public ActionResult Index()
        {
            MenuVM menuVM = new MenuVM()
            {
                Account = "menu-open",
                Account_Main = "active",
                Account_Role = "active"
            };
            return View(menuVM);
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            return Json(roleBusiness.GetAll(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Insert()
        {
            MenuVM menuVM = new MenuVM()
            {
                Account = "menu-open",
                Account_Main = "active",
                Account_Role = "active"
            };

            ViewBag.menuVM = menuVM;

            return View();
        }

        [HttpPost]
        public ActionResult Insert(RoleVM roleVM)
        {
            Role role = new Role()
            {
                RoleName = roleVM.RoleName,
                Description = roleVM.Description
            };

            roleBusiness.Insert(role);
            return RedirectToAction("Index", "Role");
        }

        [HttpGet]
        public ActionResult Update(int Id)
        {
            MenuVM menuVM = new MenuVM()
            {
                Account = "menu-open",
                Account_Main = "active",
                Account_Role = "active"
            };

            ViewBag.menuVM = menuVM;

            Role role = roleBusiness.GetById(Id);
            RoleVM roleVM = new RoleVM()
            {
                RoleName = role.RoleName,
                Description = role.Description
            };

            return View(roleVM);
        }

        [HttpPost]
        public ActionResult Update(RoleVM roleVM)
        {
            Role role = new Role()
            {
                Id = roleVM.Id,
                RoleName = roleVM.RoleName,
                Description = roleVM.Description
            };

            roleBusiness.Update(role);
            return RedirectToAction("Index", "Role");
        }

        [HttpGet]
        public ActionResult Grant(int Id)
        {
            MenuVM menuVM = new MenuVM()
            {
                Account = "menu-open",
                Account_Main = "active",
                Account_Role = "active"
            };

            ViewBag.menuVM = menuVM;

            Role role = roleBusiness.GetById(Id);
            RoleVM roleVM = new RoleVM()
            {
                RoleName = role.RoleName,
                Description = role.Description
            };

            return View(roleVM);
        }

        [HttpPost]
        public ActionResult Grant(RoleVM roleVM)
        {
            Role role = new Role()
            {
                Id = roleVM.Id,
                RoleName = roleVM.RoleName,
                Description = roleVM.Description
            };

            roleBusiness.Update(role);
            return RedirectToAction("Index", "Role");
        }

        [HttpGet]
        public ActionResult Delete(string Id)
        {
            return RedirectToAction("Index", "Role");
        }

        [HttpGet]
        public ActionResult MultipleDelete()
        {
            return RedirectToAction("Index", "Role");
        }
    }
}