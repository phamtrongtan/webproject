﻿using BlogWebsite.Administration.Models.ViewModel;
using BlogWebsite.Data.Business.Stories;
using BlogWebsite.Data.Models.Stories;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Controllers
{
    public class StoryContentController : Controller
    {
        StoryContentBusiness storyContentBusiness;

        public StoryContentController()
        {
            storyContentBusiness = new StoryContentBusiness();
        }

        [HttpGet]
        public ActionResult Insert()
        {
            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Read = "active"
            };
            ViewBag.menuVM = menuVM;

            return View();
        }

        [HttpPost]
        public ActionResult Insert(StoryContentVM storyContentVM)
        {
            var sanitizedHtml = Sanitizer.GetSafeHtmlFragment(storyContentVM.Content);
            int storyId = (int)Session["StoryId"];

            storyContentBusiness.Insert(new StoryContent()
            {
                StoryId = storyId,
                ChapterName = storyContentVM.ChapterName,
                Content = sanitizedHtml
            });

            return RedirectToAction("Contents", "Story", new { id = storyId });
        }

        [HttpGet]
        public ActionResult Update(long Id)
        {
            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Read = "active"
            };
            ViewBag.menuVM = menuVM;

            StoryContent storyContent = storyContentBusiness.GetById(Id);
            StoryContentVM storyContentVM = new StoryContentVM()
            {
                Id = storyContent.Id,
                StoryId = storyContent.StoryId,
                ChapterName = storyContent.ChapterName,
                Content = storyContent.Content
            };

            return View(storyContentVM);
        }

        [HttpPost]
        public ActionResult Update(StoryContentVM storyContentVM)
        {
            int storyId = (int)Session["StoryId"];

            storyContentBusiness.Update(new StoryContent()
            {
                Id = storyContentVM.Id,
                StoryId = storyId,
                ChapterName = storyContentVM.ChapterName,
                Content = storyContentVM.Content
            });
            return RedirectToAction("Contents", "Story", new { id = storyId });
        }

        [HttpGet]
        public ActionResult Delete(long Id)
        {
            storyContentBusiness.Delete(Id);
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult MultipleDelete()
        {
            return RedirectToAction("Contents", "Story");
        }

        [HttpGet]
        public ActionResult Watch(long Id)
        {
            StoryContent storyContent = storyContentBusiness.GetById(Id);
            StoryContentVM storyContentVM = new StoryContentVM()
            {
                ChapterName = storyContent.ChapterName,
                Content = storyContent.Content
            };

            return PartialView(storyContentVM);
        }
    }
}