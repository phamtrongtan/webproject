﻿using BlogWebsite.Administration.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Controllers
{
    public class StoryComicsController : Controller
    {
        public ActionResult Index()
        {
            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Comics = "active"
            };
            return View(menuVM);
        }
    }
}