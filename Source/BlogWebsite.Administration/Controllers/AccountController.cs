﻿using BlogWebsite.Administration.Models.ViewModel;
using BlogWebsite.Data.Business.Account;
using BlogWebsite.Data.Models.Account;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Controllers
{
    public class AccountController : Controller
    {
        UserBusiness userBusiness;

        public AccountController()
        {
            userBusiness = new UserBusiness();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginVM login)
        {
            User user = userBusiness.CheckLogin(login.UserName, login.Password);

            if (user != null)
            {
                Session["UserName"] = login.UserName;
                userBusiness.UpdateLoginDate(login.UserName);

                return RedirectToAction("Index", "Home");
            }

            TempData["LoginMessage"] = "No";

            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterVM register)
        {
            // Validate form
            if (string.IsNullOrWhiteSpace(register.UserName) || string.IsNullOrWhiteSpace(register.Email) || string.IsNullOrWhiteSpace(register.Password) || string.IsNullOrWhiteSpace(register.RePassword))
            {
                TempData["RegisterMessage"] = "Vui lòng điền đủ tất cả thông tin!";
                return RedirectToAction("Register", "Account");
            }

            // Kiểm tra Password và RePassword
            if (register.Password != register.RePassword)
            {
                TempData["RegisterMessage"] = "Mật khẩu xác nhận chưa khớp!";
                return RedirectToAction("Register", "Account");
            }

            // Kiểm tra sự tồn tại duy nhất của UserName và Email
            User user = userBusiness.GetByUserName(register.UserName);
            if (user == null)
            {
                User email = userBusiness.GetByEmail(register.Email);
                if (email == null)
                {
                    userBusiness.Register(register.UserName, register.Password, register.Email);
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    TempData["RegisterMessage"] = "Email đã tồn tại trong hệ thống!";
                }
            }
            else
            {
                TempData["RegisterMessage"] = "Tên đăng nhập đã tồn tại trong hệ thống!";
            }            

            return RedirectToAction("Register", "Account");
        }

        [HttpGet]
        public ActionResult ForgetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgetPassword(ForgetPasswordVM forget)
        {
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult LogOut()
        {
            Session["UserName"] = null;
            return RedirectToAction("Login", "Account");
        }
    }
}