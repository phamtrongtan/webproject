﻿using BlogWebsite.Administration.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Controllers
{
    public class StoryAudioContentController : Controller
    {
        [HttpGet]
        public ActionResult Insert()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Watch(string id)
        {
            StoryAudioContentVM chapterContent = null;

            //using (MyBlogEntities entity = new MyBlogEntities())
            //{
            //    var lst = entity.SP_StoryContent_GetById(2);

            //    foreach (var item in lst)
            //    {
            //        chapterContent = new StoryAudioContentVM()
            //        {
            //            ChapterName = item.ChapterName,
            //            //Content = item.Content
            //        };
            //    }
            //}

            return PartialView(chapterContent);
        }

        [HttpPost]
        public ActionResult Insert( HttpPostedFileBase AudioPath, StoryAudioContentVM chapterContent)
        {
            try
            {
                if (AudioPath.ContentLength > 0)
                {
                    string filename = Path.GetFileName(AudioPath.FileName);
                    string path = Path.Combine(Server.MapPath("~/Assets/Upload"), filename);                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            //using (MyBlogEntities entity = new MyBlogEntities())
            //{
            //    //entity.SP_StoryContent_Insert(1, chapterContent.ChapterIndex, chapterContent.ChapterName, chapterContent.Content);
            //}

            return RedirectToAction("Contents", "StoryAudio");
        }

        [HttpGet]
        public ActionResult Update(string Id)
        {
            StoryAudioContentVM chapterContent = null;

            //using (MyBlogEntities entity = new MyBlogEntities())
            //{
            //    var lst = entity.SP_StoryContent_GetById(1);

            //    foreach (var item in lst)
            //    {
            //        chapterContent = new StoryAudioContentVM()
            //        {
            //            ChapterIndex = item.ChapterIndex,
            //            ChapterName = item.ChapterName,
            //            //Content = item.Content
            //        };
            //    }
            //}

            return View(chapterContent);
        }

        [HttpPost]
        public ActionResult Update(StoryAudioContentVM chapterContent)
        {
            return RedirectToAction("Contents", "StoryAudio");
        }

        [HttpGet]
        public ActionResult Delete(string Id)
        {
            return RedirectToAction("Contents", "StoryAudio");
        }

        [HttpGet]
        public ActionResult MultipleDelete()
        {
            return RedirectToAction("Contents", "StoryAudio");
        }

        [HttpPost]
        public string UploadProcess(HttpPostedFileBase file)
        {
            //string filePath = Server.MapPath( "~/Assets/Libs/" + file.FileName);
            string filePath = file.FileName;

            return filePath;
        }
    }
}