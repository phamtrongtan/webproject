﻿using BlogWebsite.Administration.Models.ViewModel;
using BlogWebsite.Data.Business.Stories;
using BlogWebsite.Data.Models.Stories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Controllers
{
    public class StoryTypeController : Controller
    {
        StoryTypeBusiness storyTypeBusiness;
        public StoryTypeController()
        {
            storyTypeBusiness = new StoryTypeBusiness();
        }

        [HttpGet]
        public ActionResult Index()
        {
            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Type = "active"
            };
            return View(menuVM);
        }

        [HttpPost]
        public JsonResult Search(string keyword)
        {            
            return Json(storyTypeBusiness.Search(keyword), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Insert()
        {
            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Type = "active"
            };
            ViewBag.menuVM = menuVM;

            return View();
        }

        [HttpPost]
        public ActionResult Insert(StoryTypeVM storyType)
        {
            storyTypeBusiness.Insert(new StoryType() {
                StoryTypeName = storyType.StoryTypeName,
                Description = storyType.Description
            });

            return RedirectToAction("Index", "StoryType");
        }

        [HttpGet]
        public ActionResult Update(int Id)
        {
            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Type = "active"
            };
            ViewBag.menuVM = menuVM;

            StoryType storyType = storyTypeBusiness.GetById(Id);
            StoryTypeVM storyTypeVM = new StoryTypeVM()
            {
                Id = storyType.Id,
                StoryTypeName = storyType.StoryTypeName,
                Description = storyType.Description
            };
            return View(storyTypeVM);
        }

        [HttpPost]
        public ActionResult Update(StoryTypeVM storyType)
        {
            storyTypeBusiness.Update(new StoryType() {
                Id = storyType.Id,
                StoryTypeName = storyType.StoryTypeName,
                Description = storyType.Description
            });
            return RedirectToAction("Index", "StoryType");
        }

        [HttpGet]
        public ActionResult Delete(int Id)
        {
            storyTypeBusiness.Delete(Id);
            return RedirectToAction("Index", "StoryType");
        }

        [HttpGet]
        public ActionResult MultipleDelete()
        {
            return RedirectToAction("Index", "StoryType");
        }
    }
}