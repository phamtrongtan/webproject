﻿using BlogWebsite.Administration.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Controllers
{
    public class PermissionController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            MenuVM menuVM = new MenuVM()
            {
                Account = "menu-open",
                Account_Main = "active",
                Account_Permission = "active"
            };
            return View(menuVM);
        }

        [HttpGet]
        public ActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Insert(PermissionVM user)
        {
            return RedirectToAction("Index", "Permission");
        }

        [HttpGet]
        public ActionResult Update(string Id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Update(PermissionVM user)
        {
            return RedirectToAction("Index", "Permission");
        }

        [HttpGet]
        public ActionResult Delete(string Id)
        {
            return RedirectToAction("Index", "Permission");
        }

        [HttpGet]
        public ActionResult MultipleDelete()
        {
            return RedirectToAction("Index", "Permission");
        }
    }
}