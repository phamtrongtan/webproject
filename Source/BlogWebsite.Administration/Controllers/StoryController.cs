﻿using BlogWebsite.Administration.Models.ViewModel;
using BlogWebsite.Data.Business.Stories;
using BlogWebsite.Data.Models.Stories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Controllers
{
    public class StoryController : Controller
    {
        StoryTypeBusiness storyTypeBusiness;
        StoryBusiness storyBusiness;
        StoryContentBusiness storyContentBusiness;

        public StoryController()
        {
            storyTypeBusiness = new StoryTypeBusiness();
            storyBusiness = new StoryBusiness();
            storyContentBusiness = new StoryContentBusiness();
        }

        [HttpGet]
        public ActionResult Index()
        {
            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Read = "active"
            };
            return View(menuVM);
        }

        [HttpPost]
        public JsonResult Search(string keyword)
        {
            return Json(storyBusiness.Search(keyword), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Insert()
        {
            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Read = "active"
            };
            ViewBag.menuVM = menuVM;

            List<SelectListItem> lstStoryType = new List<SelectListItem>();

            foreach (var item in storyTypeBusiness.GetAll())
            {
                lstStoryType.Add(new SelectListItem() { Text = item.StoryTypeName, Value = item.Id.ToString() });
            }
            ViewBag.lstStoryType = lstStoryType;

            return View();
        }

        [HttpPost]
        public ActionResult Insert(HttpPostedFileBase Picture, StoryVM story)
        {
            string pictureName = "";

            if (Picture != null && Picture.ContentLength > 0)
            {
                pictureName = Picture.FileName;
                string filepath = Path.Combine(Server.MapPath("~/Assets/Upload/Story"), pictureName);
                Picture.SaveAs(filepath);
            }

            storyBusiness.Insert(new Story()
            {
                StoryTypeId = story.StoryTypeId,
                StoryName = story.StoryName,
                Author = story.Author,
                ChapterNumber = story.ChapterNumber,
                Introduction = story.Introduction,
                Picture = pictureName,
                Status = story.Status
            });

            return RedirectToAction("Index", "Story");
        }

        [HttpGet]
        public ActionResult Update(int Id)
        {
            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Read = "active"
            };
            ViewBag.menuVM = menuVM;
            ViewBag.lstStoryType = storyTypeBusiness.GetAll();

            Story story = storyBusiness.GetById(Id);
            StoryVM storyVM = new StoryVM()
            {
                Id = story.Id,
                StoryTypeId = story.StoryTypeId,
                StoryName = story.StoryName,
                Author = story.Author,
                ChapterNumber = story.ChapterNumber,
                Introduction = story.Introduction,
                Picture = story.Picture,
                Status = story.Status
            };

            return View(storyVM);
        }

        [HttpPost]
        public ActionResult Update(HttpPostedFileBase Picture, StoryVM story)
        {
            string pictureName = "";

            if (Picture != null && Picture.ContentLength > 0)
            {
                pictureName = Picture.FileName;
                string filepath = Path.Combine(Server.MapPath("~/Assets/Upload/Story"), pictureName);
                Picture.SaveAs(filepath);
            }

            if (pictureName == "")
            {
                storyBusiness.UpdateExceptPicture(new Story()
                {
                    Id = story.Id,
                    StoryTypeId = story.StoryTypeId,
                    StoryName = story.StoryName,
                    Author = story.Author,
                    ChapterNumber = story.ChapterNumber,
                    Introduction = story.Introduction,
                    Status = story.Status
                });
            }
            else
            {
                storyBusiness.Update(new Story()
                {
                    Id = story.Id,
                    StoryTypeId = story.StoryTypeId,
                    StoryName = story.StoryName,
                    Author = story.Author,
                    ChapterNumber = story.ChapterNumber,
                    Introduction = story.Introduction,
                    Picture = pictureName,
                    Status = story.Status
                });
            }

            return RedirectToAction("Index", "Story");
        }

        [HttpGet]
        public ActionResult Delete(int Id)
        {
            storyBusiness.Delete(Id);
            return RedirectToAction("Index", "Story");
        }

        [HttpGet]
        public ActionResult MultipleDelete()
        {
            return RedirectToAction("Index", "Story");
        }

        [HttpGet]
        public ActionResult Contents(int Id)
        {
            Session["StoryId"] = Id;
            Session["StoryName"] = storyBusiness.GetById(Id).StoryName;

            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Read = "active"
            };
            return View(menuVM);
        }

        [HttpPost]
        public JsonResult Contents_Index(string keyword)
        {
            int storyId = (int)Session["StoryId"];
            return Json(storyContentBusiness.Search(storyId, keyword), JsonRequestBehavior.AllowGet);
        }
    }
}