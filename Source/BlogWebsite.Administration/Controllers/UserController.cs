﻿using BlogWebsite.Administration.Models.Core;
using BlogWebsite.Administration.Models.ViewModel;
using BlogWebsite.Data.Business.Account;
using BlogWebsite.Data.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Controllers
{
    public class UserController : Controller
    {
        UserBusiness userBusiness;
        RoleBusiness roleBusiness;

        public UserController()
        {
            userBusiness = new UserBusiness();
            roleBusiness = new RoleBusiness();
        }

        [HttpGet]
        public ActionResult Index()
        {
            MenuVM menuVM = new MenuVM()
            {
                Account = "menu-open",
                Account_Main = "active",
                Account_User = "active"
            };
            return View(menuVM);
        }

        [HttpGet]
        public JsonResult GetAll()
        {
            return Json(userBusiness.GetAll(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Insert()
        {
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Insert(UserVM user)
        {
            return RedirectToAction("Index", "User");
        }

        [HttpGet]
        public ActionResult Update(int Id)
        {
            MenuVM menuVM = new MenuVM()
            {
                Account = "menu-open",
                Account_Main = "active",
                Account_User = "active"
            };

            ViewBag.menuVM = menuVM;
            ViewBag.lstRole = roleBusiness.GetAll();

            // Lấy danh sách UserStatus
            int index = 1;
            List<SelectListItem> lstUserStatus = new List<SelectListItem>();
            foreach (string item in UserStatus.Status)
            {
                lstUserStatus.Add(new SelectListItem() { Text = item, Value = index.ToString() });
                index++;
            }
            ViewBag.lstUserStatus = lstUserStatus;

            User user = userBusiness.GetById(Id);
            UserVM userVM = new UserVM()
            {
                RoleId = user.RoleId,
                UserName = user.UserName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Address = user.Address,
                Status = 1
            };                       

            return View(userVM);
        }

        [HttpPost]
        public ActionResult Update(UserVM userVM)
        {
            User user = new User()
            {
                Id = userVM.Id,
                RoleId = userVM.RoleId,
                Email = userVM.Email,
                PhoneNumber = userVM.PhoneNumber,
                Address = userVM.Address,
                Status = userVM.Status
            };

            userBusiness.Update(user);

            return RedirectToAction("Index", "User");
        }

        [HttpGet]
        public ActionResult Delete(string Id)
        {
            return RedirectToAction("Index", "User");
        }

        [HttpGet]
        public ActionResult MultipleDelete()
        {
            return RedirectToAction("Index", "User");
        }
    }
}