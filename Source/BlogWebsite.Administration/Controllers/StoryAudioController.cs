﻿using BlogWebsite.Administration.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Controllers
{
    public class StoryAudioController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Audio = "active"
            };
            return View(menuVM);
        }

        [HttpGet]
        public ActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Insert(StoryAudioVM storyAudio)
        {
            return RedirectToAction("Index", "StoryAudio");
        }

        [HttpGet]
        public ActionResult Update(string Id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Update(StoryAudioVM storyAudio)
        {
            return RedirectToAction("Index", "StoryAudio");
        }

        [HttpGet]
        public ActionResult Delete(string Id)
        {
            return RedirectToAction("Index", "StoryAudio");
        }

        [HttpGet]
        public ActionResult MultipleDelete()
        {
            return RedirectToAction("Index", "StoryAudio");
        }

        [HttpGet]
        public ActionResult Contents(string Id)
        {
            MenuVM menuVM = new MenuVM()
            {
                Story = "menu-open",
                Story_Main = "active",
                Story_Audio = "active"
            };
            return View(menuVM);
        }
    }
}