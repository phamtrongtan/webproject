﻿using BlogWebsite.Administration.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            MenuVM menuVM = new MenuVM()
            {
                Home = "active"
            };
            return View(menuVM);
        }
    }
}