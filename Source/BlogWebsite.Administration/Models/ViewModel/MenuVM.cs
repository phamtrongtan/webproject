﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogWebsite.Administration.Models.ViewModel
{
    public class MenuVM
    {
        public string Home { get; set; }
        public string Account { get; set; }
        public string Account_Main { get; set; }
        public string Account_User { get; set; }
        public string Account_Role { get; set; }
        public string Account_Permission { get; set; }
        public string Story { get; set; }
        public string Story_Main { get; set; }
        public string Story_Type { get; set; }
        public string Story_Read { get; set; }
        public string Story_Audio { get; set; }
        public string Story_Comics { get; set; }
    }
}