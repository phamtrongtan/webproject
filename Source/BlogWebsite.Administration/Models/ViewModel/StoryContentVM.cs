﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Models.ViewModel
{
    public class StoryContentVM
    {
        public long Id { get; set; }
        public int StoryId { get; set; }
        public string ChapterName { get; set; }

        [AllowHtml]
        public string Content { get; set; }
    }
}