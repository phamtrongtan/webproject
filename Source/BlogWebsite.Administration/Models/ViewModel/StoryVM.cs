﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogWebsite.Administration.Models.ViewModel
{ 
    public class StoryVM
    {
        public int Id { get; set; }
        public int StoryTypeId { get; set; }
        public string StoryName { get; set; }
        public string Author { get; set; }

        [AllowHtml]
        public string Introduction { get; set; }
        public int ChapterNumber { get; set; }
        public string Status { get; set; }
        public string Picture { get; set; }
    }
}