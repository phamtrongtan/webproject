﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogWebsite.Administration.Models.ViewModel
{
    public class RoleVM
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
    }
}