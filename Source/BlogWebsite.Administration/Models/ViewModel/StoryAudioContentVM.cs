﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogWebsite.Administration.Models.ViewModel
{
    public class StoryAudioContentVM
    {
        public int Id { get; set; }
        public string ChapterName { get; set; }
        public string AudioPath { get; set; }
    }
}