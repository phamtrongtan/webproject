﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogWebsite.Administration.Models.ViewModel
{
    public class LoginVM
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class RegisterVM
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
    }

    public class ForgetPasswordVM
    {
        public string Email { get; set; }
    }
}