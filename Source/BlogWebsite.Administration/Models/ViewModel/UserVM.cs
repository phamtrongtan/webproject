﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogWebsite.Administration.Models.ViewModel
{
    public class UserVM
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public short Status { get; set; }
    }
}