﻿using BlogWebsite.Data.EF;
using BlogWebsite.Data.Models.Stories;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;

namespace BlogWebsite.Data.Business.Stories
{
    public class StoryTypeBusiness
    {
        public IEnumerable<StoryType> GetAll()
        {
            List<StoryType> lstStoryType = new List<StoryType>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryType_GetAll();

                foreach (var item in lst)
                {
                    lstStoryType.Add(new StoryType() {
                        Id = item.Id,
                        StoryTypeName = item.StoryTypeName,
                        Description = item.Description
                    });
                }
            }

            return lstStoryType;
        }

        public StoryType GetById(int Id)
        {
            StoryType storyType = null;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryType_GetById(Id);

                foreach (var item in lst)
                {
                    storyType = new StoryType()
                    {
                        Id = item.Id,
                        StoryTypeName = item.StoryTypeName,
                        Description = item.Description
                    };
                }
            }

            return storyType;
        }

        public int Delete(int Id)
        {
            int count = 0;
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryType_Delete(Id);
                foreach (int item in lst)
                {
                    count = item;
                }
            }

            return count;
        }

        public int Insert(StoryType storyType)
        {
            int count = 0;
            ObjectParameter output = new ObjectParameter("Id", typeof(int));

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryType_Insert(storyType.StoryTypeName, storyType.Description, output);
                foreach (int item in lst)
                {
                    count = item;
                }
            }

            int index = (int)output.Value;

            return count;
        }

        public int Update(StoryType storyType)
        {
            int count = 0;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryType_Update( storyType.Id, storyType.StoryTypeName, storyType.Description);
                foreach (int item in lst)
                {
                    count = item;
                }
            }

            return count;
        }

        public IEnumerable<StoryType> Search(string keyword)
        {
            List<StoryType> lstStoryType = new List<StoryType>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryType_Search(keyword);

                foreach (var item in lst)
                {
                    lstStoryType.Add(new StoryType()
                    {
                        Id = item.Id,
                        StoryTypeName = item.StoryTypeName,
                        Description = item.Description
                    });
                }
            }

            return lstStoryType;
        }
    }
}
