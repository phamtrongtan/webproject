﻿using BlogWebsite.Data.EF;
using BlogWebsite.Data.Models.Stories;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWebsite.Data.Business.Stories
{
    public class StoryContentBusiness
    {
        public IEnumerable<StoryContent> GetAll()
        {
            List<StoryContent> lstStoryContent = new List<StoryContent>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryContent_GetAll();

                foreach (var item in lst)
                {
                    lstStoryContent.Add(new StoryContent()
                    {
                        Id = item.Id,
                        StoryId = item.StoryId,
                        ChapterName = item.ChapterName,
                        Content = item.Content
                    });
                }
            }

            return lstStoryContent;
        }

        public IEnumerable<StoryContent> GetByStoryId(int storyId)
        {
            List<StoryContent> lstStoryContent = new List<StoryContent>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryContent_GetByStoryId(storyId);

                foreach (var item in lst)
                {
                    lstStoryContent.Add(new StoryContent()
                    {
                        Id = item.Id,
                        StoryId = item.StoryId,
                        ChapterName = item.ChapterName,
                        Content = item.Content
                    });
                }
            }

            return lstStoryContent;
        }

        public StoryContent GetById(long Id)
        {
            StoryContent storyContent = null;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryContent_GetById(Id);

                foreach (var item in lst)
                {
                    storyContent = new StoryContent()
                    {
                        Id = item.Id,
                        StoryId = item.StoryId,
                        ChapterName = item.ChapterName,
                        Content = item.Content
                    };
                }
            }

            return storyContent;
        }

        public int Delete(long Id)
        {
            int count = 0;
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryContent_Delete(Id);
                foreach (int item in lst)
                {
                    count = item;
                }
            }

            return count;
        }

        public int Insert(StoryContent storyContent)
        {
            int count = 0;
            ObjectParameter output = new ObjectParameter("Id", typeof(long));

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryContent_Insert(storyContent.StoryId, storyContent.ChapterName, storyContent.Content, output);
                foreach (int item in lst)
                {
                    count = item;
                }
            }

            long index = (long)output.Value;

            return count;
        }

        public int Update(StoryContent storyContent)
        {
            int count = 0;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryContent_Update(storyContent.Id, storyContent.StoryId, storyContent.ChapterName, storyContent.Content);
                foreach (int item in lst)
                {
                    count = item;
                }
            }

            return count;
        }

        public IEnumerable<StoryContent> Search(int storyId, string keyword)
        {
            List<StoryContent> lstStoryContent = new List<StoryContent>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_StoryContent_Search(storyId, keyword);

                foreach (var item in lst)
                {
                    lstStoryContent.Add(new StoryContent()
                    {
                        Id = item.Id,
                        StoryId = item.StoryId,
                        ChapterName = item.ChapterName,
                        Content = item.Content
                    });
                }
            }

            return lstStoryContent;
        }
    }
}
