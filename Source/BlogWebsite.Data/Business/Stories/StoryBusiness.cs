﻿using BlogWebsite.Data.EF;
using BlogWebsite.Data.Models.Stories;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWebsite.Data.Business.Stories
{
    public class StoryBusiness
    {
        public IEnumerable<Story> GetAll()
        {
            List<Story> lstStory = new List<Story>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_Story_GetAll();

                foreach (var item in lst)
                {
                    lstStory.Add(new Story()
                    {
                        Id = item.Id,
                        StoryTypeId = (int)item.StoryTypeId,
                        StoryTypeName = item.StoryTypeName,
                        StoryName = item.StoryName,
                        Author = item.Author,
                        ChapterNumber = (int)item.ChapterNumber,
                        Introduction = item.Introduction,
                        Picture = item.Picture,
                        Status = item.Status
                    });
                }
            }

            return lstStory;
        }

        public Story GetById(int Id)
        {
            Story story = null;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_Story_GetById(Id);

                foreach (var item in lst)
                {
                    story = new Story()
                    {
                        Id = item.Id,
                        StoryTypeId = (int)item.StoryTypeId,
                        StoryTypeName = item.StoryTypeName,
                        StoryName = item.StoryName,
                        Author = item.Author,
                        ChapterNumber = (int)item.ChapterNumber,
                        Introduction = item.Introduction,
                        Picture = item.Picture,
                        Status = item.Status
                    };
                }
            }

            return story;
        }

        public int Delete(int Id)
        {
            int count = 0;
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_Story_Delete(Id);
                foreach (int item in lst)
                {
                    count = item;
                }
            }

            return count;
        }

        public int Insert(Story story)
        {
            int count = 0;
            ObjectParameter output = new ObjectParameter("Id", typeof(int));

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_Story_Insert( story.StoryTypeId, story.StoryName, story.Author, story.Introduction, story.ChapterNumber, story.Status, story.Picture, output);
                foreach (int item in lst)
                {
                    count = item;
                }
            }

            int index = (int)output.Value;

            return count;
        }

        public int Update(Story story)
        {
            int count = 0;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_Story_Update(story.Id, story.StoryTypeId, story.StoryName, story.Author, story.Introduction, story.ChapterNumber, story.Status, story.Picture);
                foreach (int item in lst)
                {
                    count = item;
                }
            }

            return count;
        }

        public int UpdateExceptPicture(Story story)
        {
            int count = 0;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_Story_UpdateExceptPicture(story.Id, story.StoryTypeId, story.StoryName, story.Author, story.Introduction, story.ChapterNumber, story.Status);
                foreach (int item in lst)
                {
                    count = item;
                }
            }

            return count;
        }

        public IEnumerable<Story> Search(string keyword)
        {
            List<Story> lstStory = new List<Story>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_Story_Search(keyword);

                foreach (var item in lst)
                {
                    lstStory.Add(new Story()
                    {
                        Id = item.Id,
                        StoryTypeId = (int)item.StoryTypeId,
                        StoryTypeName = item.StoryTypeName,
                        StoryName = item.StoryName,
                        Author = item.Author,
                        ChapterNumber = (int)item.ChapterNumber,
                        Introduction = item.Introduction,
                        Picture = item.Picture,
                        Status = item.Status
                    });
                }
            }

            return lstStory;
        }
    }
}
