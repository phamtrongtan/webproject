﻿using BlogWebsite.Data.EF;
using BlogWebsite.Data.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWebsite.Data.Business.Account
{
    public class PermissionBusiness
    {
        public IEnumerable<Permission> GetAll()
        {
            List<Permission> lstPermission = new List<Permission>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_Permission_GetAll();

                foreach (var item in lst)
                {
                    lstPermission.Add(new Permission()
                    {
                        Id = item.Id,
                        PermissionName = item.PermissionName,
                        Description = item.Description
                    });
                }
            }

            return lstPermission;
        }

        public Permission GetById(int Id)
        {
            Permission permission = null;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_Permission_GetById(Id);

                foreach (var item in lst)
                {
                    permission = new Permission()
                    {
                        Id = item.Id,
                        PermissionName = item.PermissionName,
                        Description = item.Description
                    };
                }
            }

            return permission;
        }

        public void Delete(int Id)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_Permission_Delete(Id);
            }
        }

        public void Insert(Permission permission)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_Permission_Insert(permission.PermissionName, permission.Description);
            }
        }

        public void Update(Permission permission)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_Permission_Update(permission.Id, permission.PermissionName, permission.Description);
            }
        }
    }
}
