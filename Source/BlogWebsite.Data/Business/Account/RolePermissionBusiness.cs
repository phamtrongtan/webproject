﻿using BlogWebsite.Data.EF;
using BlogWebsite.Data.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWebsite.Data.Business.Account
{
    public class RolePermissionPermissionBusiness
    {
        public IEnumerable<RolePermission> GetAll(int roleId)
        {
            List<RolePermission> lstRolePermission = new List<RolePermission>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_RolePermission_GetAll();

                foreach (var item in lst)
                {
                    lstRolePermission.Add(new RolePermission()
                    {
                        RoleId = item.RoleId,
                        PermissionId = item.PermissionId
                    });
                }
            }

            return lstRolePermission;
        }

        public IEnumerable<RolePermission> GetByRoleId(int roleId)
        {
            List<RolePermission> lstRolePermission = new List<RolePermission>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_RolePermission_GetByRoleId(roleId);

                foreach (var item in lst)
                {
                    lstRolePermission.Add(new RolePermission()
                    {
                        RoleId = item.RoleId,
                        PermissionId = item.PermissionId
                    });
                }
            }

            return lstRolePermission;
        }

        public void DeleteByRoleId(int roleId)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_RolePermission_DeleteByRoleId(roleId);
            }
        }

        public void Insert(RolePermission rolePermission)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_RolePermission_Insert(rolePermission.RoleId, rolePermission.PermissionId);
            }
        }
    }
}
