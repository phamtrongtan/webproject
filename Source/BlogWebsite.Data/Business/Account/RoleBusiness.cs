﻿using BlogWebsite.Data.EF;
using BlogWebsite.Data.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWebsite.Data.Business.Account
{
    public class RoleBusiness
    {
        public IEnumerable<Role> GetAll()
        {
            List<Role> lstRole = new List<Role>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_Role_GetAll();

                foreach (var item in lst)
                {
                    lstRole.Add(new Role()
                    {
                        Id = item.Id,
                        RoleName = item.RoleName,
                        Description = item.Description
                    });
                }
            }

            return lstRole;
        }

        public Role GetById(int Id)
        {
            Role role = null;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_Role_GetById(Id);

                foreach (var item in lst)
                {
                    role = new Role()
                    {
                        Id = item.Id,
                        RoleName = item.RoleName,
                        Description = item.Description
                    };
                }
            }

            return role;
        }

        public void Delete(int Id)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_Role_Delete(Id);
            }
        }

        public void Insert(Role role)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_Role_Insert(role.RoleName, role.Description);
            }
        }

        public void Update(Role role)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_Role_Update(role.Id, role.RoleName, role.Description);
            }
        }
    }
}
