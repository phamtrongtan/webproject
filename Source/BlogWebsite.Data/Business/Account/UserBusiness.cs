﻿using BlogWebsite.Data.EF;
using BlogWebsite.Data.Models.Account;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWebsite.Data.Business.Account
{
    public class UserBusiness
    {
        public IEnumerable<User> GetAll()
        {
            List<User> lstUser = new List<User>();

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_User_GetAll();

                foreach (var item in lst)
                {
                    lstUser.Add(new User()
                    {
                        Id = item.Id,
                        RoleId = item.RoleId,
                        UserName = item.UserName,
                        Email = item.Email,
                        Password = item.Password,
                        PhoneNumber = item.PhoneNumber,
                        Address = item.Address,
                        Status = item.Status,
                        CreatedDate = (DateTime)item.CreatedDate,
                        LoginDate = (DateTime)item.LoginDate
                    });
                }
            }

            return lstUser;
        }

        public User GetById(int Id)
        {
            User user = null;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_User_GetById(Id);

                foreach (var item in lst)
                {
                    user = new User()
                    {
                        Id = item.Id,
                        RoleId = item.RoleId,
                        UserName = item.UserName,
                        Email = item.Email,
                        Password = item.Password,
                        PhoneNumber = item.PhoneNumber,
                        Address = item.Address,
                        Status = item.Status,
                        CreatedDate = (DateTime)item.CreatedDate,
                        LoginDate = (DateTime)item.LoginDate
                    };
                }
            }

            return user;
        }

        public User GetByEmail(string email)
        {
            User user = null;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_User_GetByEmail(email);

                foreach (var item in lst)
                {
                    user = new User()
                    {
                        Id = item.Id,
                        RoleId = item.RoleId,
                        UserName = item.UserName,
                        Email = item.Email,
                        Password = item.Password,
                        PhoneNumber = item.PhoneNumber,
                        Address = item.Address,
                        Status = item.Status,
                        CreatedDate = (DateTime)item.CreatedDate,
                        LoginDate = (DateTime)item.LoginDate
                    };
                }
            }

            return user;
        }

        public User GetByUserName(string username)
        {
            User user = null;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                var lst = entity.SP_User_GetByUserName(username);

                foreach (var item in lst)
                {
                    user = new User()
                    {
                        Id = item.Id,
                        RoleId = item.RoleId,
                        UserName = item.UserName,
                        Email = item.Email,
                        Password = item.Password,
                        PhoneNumber = item.PhoneNumber,
                        Address = item.Address,
                        Status = item.Status,
                        CreatedDate = (DateTime)item.CreatedDate,
                        LoginDate = (DateTime)item.LoginDate
                    };
                }
            }

            return user;
        }

        public User CheckLogin(string username, string password)
{
            User user = null;

            using (MyBlogEntities entity = new MyBlogEntities())
            {
                ObjectResult<SP_User_CheckLogin_Result> lst = entity.SP_User_CheckLogin(username, password);

                foreach (var item in lst)
                {
                    user = new User()
                    {
                        Id = item.Id,
                        RoleId = item.RoleId,
                        UserName = item.UserName,
                        Email = item.Email,
                        Password = item.Password,
                        PhoneNumber = item.PhoneNumber,
                        Address = item.Address,
                        Status = item.Status
                    };
                }
            }

            return user;
        }

        public void Delete(int Id)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_User_Delete(Id);
            }
        }

        public void Register(string username, string password, string email)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_User_Register(username, password, email);
            }
        }

        public void Update(User user)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_User_Update(user.Id, user.RoleId, user.Email, user.PhoneNumber, user.Address, user.Status);
            }
        }

        public void UpdateLoginDate(string username)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_User_UpdateLoginDate(username);
            }
        }

        public void ChangePassword(string username, string password, string email)
        {
            using (MyBlogEntities entity = new MyBlogEntities())
            {
                entity.SP_User_ChangePassword(username, password, email);
            }
        }
    }
}
