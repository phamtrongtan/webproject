﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWebsite.Data.Models.Stories
{
    public class StoryAudio
    {
        public int Id { get; set; }
        public int StoryTypeId { get; set; }
        public string StoryÀudioName { get; set; }
        public string Author { get; set; }
        public string Introduction { get; set; }
        public string Picture { get; set; }
        public int ChapterNumber { get; set; }
        public string Status { get; set; }
    }
}
