﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWebsite.Data.Models.Stories
{
    public class StoryContent
    {
        public long Id { get; set; }
        public int StoryId { get; set; }
        public string ChapterName { get; set; }
        public string Content { get; set; }
    }
}
