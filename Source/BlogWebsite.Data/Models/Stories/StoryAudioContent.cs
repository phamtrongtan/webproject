﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWebsite.Data.Models.Stories
{
    public class StoryAudioContent
    {
        public long Id { get; set; }
        public int StoryAudioId { get; set; }
        public string ChapterName { get; set; }
        public string FilePath { get; set; }
    }
}
