﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogWebsite.Data.Models.Stories
{
    public class StoryType
    {
        public int Id { get; set; }
        public string StoryTypeName { get; set; }
        public string Description { get; set; }
    }
}
