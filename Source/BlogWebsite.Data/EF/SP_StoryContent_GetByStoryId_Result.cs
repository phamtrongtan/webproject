//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BlogWebsite.Data.EF
{
    using System;
    
    public partial class SP_StoryContent_GetByStoryId_Result
    {
        public long Id { get; set; }
        public int StoryId { get; set; }
        public string ChapterName { get; set; }
        public string Content { get; set; }
    }
}
